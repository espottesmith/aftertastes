Tasting Date: 01/05/2018
Tasting Location: Michael Shaps Wineworks
Tasting People: Sophia Gabrielle Weinstock
Wine: Merlot
Vintage: 2014
Producer: Michael Shaps Wineworks
Region: Charlottesville, VA, USA
Grapes: Merlot
Pairing Notes:
Paired With:
Price: $18
Serving Temperature:
Alcohol:
Appearance: Garnet, consistent into rim
Nose: Slate, graphite, raspberry, bit jammy, blackberry 
Palate: Very neutral, with tart cherry, raspberry, bit of earth
Notes: Easy drinking, but totally neutral
Rating: 1/5