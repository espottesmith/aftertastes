Tasting Date: 01/06/2018
Tasting Location: Grace Estate Winery
Tasting People: Sophia Gabrielle Weinstock
Wine: Estate Cabernet Franc
Vintage: 2014
Producer: Grace Estates Winery
Region: Charlottesville, VA, USA
Grapes: 80% cabernet franc, 10% merlot, 10% cabernet sauvignon
Pairing Notes:
Paired With:
Price:
Serving Temperature:
Alcohol:
Appearance: Touch of orange on the rim
Nose: Roasted green pepper, black pepper, kind of neutral on the nose
Palate: Medium-minus body, with jammy raspberry, blackberry, sweet tobacco
Notes: None given
Rating: None given