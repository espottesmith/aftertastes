Tasting Date: 01/05/2018
Tasting Location: Michael Shaps Wineworks
Tasting People: Sophia Gabrielle Weinstock
Wine: L. Scott
Vintage: 2013
Producer: Michael Shaps Wineworks
Region: Charlottesville, VA, USA
Grapes: Tannat, malbec, cabernet franc
Pairing Notes:
Paired With:
Price:
Serving Temperature:
Alcohol:
Appearance: Garnet, with touch of purple on rim
Nose: Vanilla, cinnamon, basil, blackberry, black currant, pomegranate, slate
Palate: Blueberry, boisenberry, touch of tobacco, cherry, currant
Notes: Super easy-drinking. Well-balanced. Tannins are soft.
Rating: 4/5