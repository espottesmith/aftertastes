Tasting Date: 01/05/2018
Tasting Location: Michael Shaps Wineworks
Tasting People: Sophia Gabrielle Weinstock
Wine: Viognier Box Wine
Vintage: N/V?
Producer: Michael Shaps Wineworks
Region: Charlottesville, VA, USA
Grapes: Viognier
Pairing Notes:
Paired With:
Price:
Serving Temperature:
Alcohol:
Appearance: Pale, translucent yellow
Nose: Neutral, but citrusy
Palate: Honeysuckle, sweet pear, sweet peach, honey
Notes: Neutral, but good for a boxed wine
Rating: 1/5