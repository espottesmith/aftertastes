Tasting Date: 01/05/2018
Tasting Location: Gabriele Rausse Winery
Tasting People: Sophia Gabrielle Weinstock
Wine: Cabernet Sauvignon Reserve
Vintage: 2015
Producer: Gabriele Rausse Winery
Region: Charlottesville, VA, USA
Grapes: Cabernet sauvignon
Pairing Notes:
Paired With:
Price: $35
Serving Temperature:
Alcohol:
Appearance: Deep blood red
Nose: Pomegranate, violet, raspberry, <>, black cherry, wet campfire, charcoal
Palate: Pomegranate, raspberry, black cherry, tart blueberry, fresh cranberry
Notes: Super fresh. Doesn't have the aging potential of earlier cab sauv. Not grippy, but high acid & quite bitter. 25% new oak, 75% neutral oak.
Rating: 3/5