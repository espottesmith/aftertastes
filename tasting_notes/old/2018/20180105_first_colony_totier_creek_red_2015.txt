Tasting Date: 01/05/2018
Tasting Location: First Colony Winery
Tasting People: Sophia Gabrielle Weinstock
Wine: Totier Creek Red
Vintage: 2015
Producer: First Colony Winery
Region: Charlottesville, VA, USA
Grapes: Petit verdot, cabernet franc, cabernet sauvignon, vidal blanc
Pairing Notes: Pizza
Paired With:
Price:
Serving Temperature:
Alcohol:
Appearance: Medium-intensity ruby
Nose: Bit smoky, sweet plum, strawberry
Palate: Bit raisiny, plum tart, strawberry
Notes: Formerly claret
Rating: 2/5