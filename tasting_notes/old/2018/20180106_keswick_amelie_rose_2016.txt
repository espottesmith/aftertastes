Tasting Date: 01/06/2018
Tasting Location: Keswick Vineyards
Tasting People: Sophia Gabrielle Weinstock
Wine: Amelie Rose
Vintage: 2016
Producer: Keswick Vineyards
Region: Charlottesville, VA, USA
Grapes: Norton
Pairing Notes:
Paired With:
Price: $35
Serving Temperature:
Alcohol:
Appearance: Deep salmon, with a hint of rouge
Nose: Cranberry, strawbery, minerally
Palate: Cranberry, crabapple, sweet blueberries, gooseberry
Notes: Super unique, but not that enjoyable. Sparkling.
Rating: 2/5