Tasting Date: 06/03/2017
Tasting Location: Black Ankle Vineyards
Tasting People: Sophia Gabrielle Weinstock, Dayle Spotte
Wine: Viento
Vintage: 2014
Producer: Black Ankle Vineyards
Region: MD, USA
Grapes: 41% cabernet franc, 41% cabernet sauvignon, 10% merlot, 8% petit verdot
Pairing Notes: Dark chocolate, filet mignon
Paired With:
Price: $34
Serving Temperature:
Alcohol:
Appearance: Garnet, light pink rim
Nose: Tobacco, chocolate, pomegranate, raspberry
Palate: Smoke, cedar, sweet black cherry
Notes: Phenomenal - could go with almost anything
Rating: 4/5