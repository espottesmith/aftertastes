Tasting Date: 06/03/2017
Tasting Location: Black Ankle Vineyards
Tasting People: Sophia Gabrielle Weinstock, Dayle Spotte
Wine: Terra Sol
Vintage: 2016
Producer: Black Ankle Vineyards
Region: MD, USA
Grapes: Gruner veltliner
Pairing Notes: Fruit platter, brie
Paired With:
Price: $30
Serving Temperature:
Alcohol:
Appearance: Pale yellow
Nose: Sweet, sugary, raising, sweet pear, kiwi
Palate: Sweet, syrup, maple, honey, honeysuckle
Notes: Amazing. Tokaji-y
Rating: 4/5