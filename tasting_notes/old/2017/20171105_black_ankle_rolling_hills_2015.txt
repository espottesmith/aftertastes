Tasting Date: 11/05/2017
Tasting Location: Black Ankle Vineyards
Tasting People: Dayle Spotte
Wine: Rolling Hills
Vintage: 2015
Producer: Black Ankle Vineyards
Region: MD, USA
Grapes: 57% cabernet sauvignon, 32% cabernet franc, 11% merlot
Pairing Notes: Prime rib, rich pork
Paired With:
Price: $38
Serving Temperature:
Alcohol:
Appearance: Deep violet to pinkish rim
Nose: Chocolate, peppercorn, green pepper, smoked wood, pomegranate, hazelnut, almond, smoke
Palate: Plummy, jammy, strawberry, cherry, blackberry, riple black plum, smoke.
Notes: Great improvement on '14. Cabs really came through - cab franc especially.
Rating: 4/5