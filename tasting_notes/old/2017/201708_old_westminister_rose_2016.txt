Tasting Date: 08/2017
Tasting Location: Old Westminister Winery
Tasting People: Sophia Gabrielle Weinstock, Dayle Spotte
Wine: Rose
Vintage: 2016
Producer: Old Westminister Winery
Region: MD, USA
Grapes: Not given
Pairing Notes:
Paired With:
Price: $28
Serving Temperature:
Alcohol:
Appearance: Salmon
Nose: Faint
Palate: White peach, plum, honey, acai
Notes:
Rating: No rating given