Tasting Date: 06/03/2017
Tasting Location: Black Ankle Vineyards
Tasting People: Dayle Spotte
Wine: Piedmont
Vintage: 2015
Producer: Black Ankle Vineyards
Region: MD, USA
Grapes: 47% gruner veltliner, 22% albarino, 28% viognier, 3% syrah
Pairing Notes: Cheeses
Paired With:
Price: $32
Serving Temperature:
Alcohol:
Appearance: Slightly cloudy, light golden
Nose: Red fruits, berries, black fruits, stone, a bit of spice, oak and smoke
Palate: Minerality, orange rind, woody, a bit earthy (unexpected)
Notes: Really interesting - want to try more
Rating: 3/5