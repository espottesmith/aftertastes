Tasting Date: 11/25/2017
Tasting Location: Black Ankle Vineyards
Tasting People: Dayle Spotte, Kyra Spotte-Smith
Wine: Crumbling Rock
Vintage: 2015
Producer: Black Ankle Vineyards
Region: MD, USA
Grapes: 28% merlot, 26% cabernet sauvignon, 24% petit verdot, 22% cabernet franc
Pairing Notes:
Paired With:
Price: $54
Serving Temperature:
Alcohol:
Appearance: Deep blood core, lightens to a raint ruby rim
Nose: Cassis, strawberry, raspberry, bit earthy, wet wood chips, tar, peppercorn, currant, pomegranate
Palate: Well balanced, light in the mouth, with medium-plus body and tannin. A bit tart, black cherry, tart plum, sour cherry. Chalk.
Notes: Not as good as previous vintages. Overshadowed by current rolling hills.
Rating: 2/5