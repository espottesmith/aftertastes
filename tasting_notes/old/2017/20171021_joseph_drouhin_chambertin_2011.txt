Tasting Date: 10/21/2017
Tasting Location: David and Bubba's apartment
Tasting People: Sophia Gabrielle Weinstock, Bubba Betty Santiago, David
Wine: Chambertin
Vintage: 2011
Producer: Joseph Drouhin
Region: Chambertin, Burgundy, France
Grapes: Pinot noir
Pairing Notes: Steak, game
Paired With: Spanish chicken, yellow rice, plantains
Price: $400
Serving Temperature:
Alcohol:
Appearance: Garnet (pale), bronze rim
Nose: Leather, spiced plum, forest floor, thyme (dried)
Palate: Slightly bitter; sharp mouthfeel. Not in balance. Tasted young - oaky, unripe red fruit.
Notes: Not worth it for the price point. Possibly too young to drink - but also definitely not a great vintage.
Rating: 2/5