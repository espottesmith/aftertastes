Tasting Date: 08/2017
Tasting Location: Old Westminister Winery
Tasting People: Sophia Gabrielle Weinstock, Dayle Spotte
Wine: Limestone
Vintage: 2016
Producer: Old Westminister Winery
Region: MD, USA
Grapes: Gruner veltliner, albarino
Pairing Notes:
Paired With:
Price: $32
Serving Temperature:
Alcohol:
Appearance: Extremely pale, egg white
Nose: White peach
Palate: Grassy, touch of minerality.
Notes: Meh. Decent blend. Typical of albarino & gruner.
Rating: 1/5